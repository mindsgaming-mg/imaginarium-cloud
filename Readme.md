## Imaginarium Cloud ![](https://cdn.glitch.com/e4863b35-a483-4826-9c93-9336d204605e%2FII_Space.png?v=1612064745191)

---

# Docs

## Join, Remix Or Embed

You can Join our creative team or remix this project:

### Join The Team

This is a coopertive cloud to join the project first become a member here

[THE INFINITE IMAGINARIUM ](https://www.minds.com/groups/profile/604673111018713103/feed)
and post about the project :P

### Remix

You can remix this project and make it your own on Glitch. You need to create an account to save the project

[Remix](https://glitch.com/edit/#!/remix/imaginarium-cloud)

## Embed In Your Website / Blogs

<code><div class="glitch-embed-wrap" style="height: 420px; width: 100%;">

  <iframe
    src="https://glitch.com/embed/#!/embed/imaginarium-cloud?path=Imaginarium/Readme.md&previewSize=100"
    title="imaginarium-cloud on Glitch"
    allow="geolocation; microphone; camera; midi; vr; encrypted-media"
    style="height: 100%; width: 100%; border: 0;">
  </iframe>
</div>
</code>

## LightWeight API

[Imaginarium API ](https://gitlab.com/mindsgaming-mg/imaginarium-api)

# Project Files

Click `Show` in the header to see your app live. Updates to your code will instantly deploy and update live.

**Glitch** is the friendly community where you'll build the app of your dreams. Glitch lets you instantly create, remix, edit, and host an app, bot or site, and you can invite collaborators or helpers to simultaneously edit code with you.

Find out more [about Glitch](https://glitch.com/about).

## Your Project

### ← README.md

That's this file, where you can tell people what your cool website does and how you built it.

### ← index.html

Where you'll write the content of your website.

### ← style.css

CSS files add styling rules to your content.

### ← script.js

If you're feeling fancy you can add interactivity to your site with JavaScript.

### ← assets

Drag in `assets`, like images or music, to add them to your project

## Built on [Glitch](https://glitch.com/) With [A-Frame](https://github.com/aframevr/aframe)

\ ゜ o ゜)ノ
