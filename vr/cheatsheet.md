# VR Challenge Cheat sheet

Welcome to the FUSE VR challenge! You can use this “Cheat Sheet” page as a helpful guide while you go through the levels of this challenge. Remember that the full instructions are always available on the [FUSE level page](https://www.fusestudio.net/challenges).


  

# Level 1  
## Terms and Code

### Sky

Default code: `<a-sky></a-sky>`

| Attribute        | Code           | Notes  |
| ------------- |:-------------:| -----:|
| `Image` | `src="360º image URL"` | Use the image URL |
| `Color` | `color="colorname"` |   Use names of colors or [hex codes](https://htmlcolorcodes.com/).  |

#### Sample code

    <a-sky src="360º image URL"></a-sky>

### Text
Default code: `<a-text></a-text>`

| Attribute        | Code           | Notes  |
| ------------- |:-------------:| -----:|
| `Value` | `value="words"` | This is the actual words that will show up. |
| `Color` | `color="colorname"` |   Use names of colors or [hex codes](https://htmlcolorcodes.com/).|
| `Rotation` | `rotation="x y z"` |  X Y Z are coordinates. For more on this go [here](https://aframe.io/docs/0.8.0/guides/building-a-basic-scene.html#transforming-an-entity-in-3d).|
| `Position` | `position="x y z"` | Move your text closer or farther away to change its size.|
| `Font` | `text="font: fontname"` | You can find a list of fonts [here](https://aframe.io/docs/0.8.0/components/text.html#fonts).|
| `Align` | `align="center" or "right"` | Set to left by default. |
| `Opacity` | `opacity="number on a scale from 0 to 1"` | 0 means fully transparent and 1 means fully opaque. |

#### Sample code

    <a-text value="Write in welcome message here"
      position="0 1 -1"></a-text>

 

# Level 2
## Terms and Code

### Text Geometry
Default code: `text-geometry="`

| Attribute        | Code           | Notes  |
| ------------- |:-------------:| -----:|
| `Value` | `value: words;` | This is the actual words that will show up.|
| `Font` | `font: #font entity id;` | Choose text geometry font from assets folder.|
| `Bevel Enabled` | `bevelEnabled: true` | Adds a beveled edge to text geometry.|
| `Bevel Size` | `bevelSize: 0.05;` | Changes size of beveled edge.|
| `Bevel Thickness` | `bevelThickeness: 0.2;` | Changes thickness of beveled edge.|
| `Curve Segments` | `curveSegments: 5;` | More curve segments make smoother geometry.|
| `Size` | `size: 1;` | Changes text geometry size.|

#### Sample code
     <a-entity position="0 1 -5" rotation="0 0 0" scale="1 1 1" 
     text-geometry="value: 3D text is cool!; font: #robotoregular; 
     bevelEnabled: true; bevelSize: 0.05; bevelThickness: 0.1; 
     curveSegments: 7; size: 1;" material="color: green; metalness: 
     0.6; sphericalEnvMap: #chrome;"></a-entity>


### Material
Default code: `material="`

| Attribute        | Code           | Notes  |
| ------------- |:-------------:| -----:|
| `Color` | `color: colorname or hexcode` |   Use names of colors or [hex codes](https://htmlcolorcodes.com/). |
| `Source` | `src: #id of texture image;` | Image used for texture map.|
| `Metalness` | `metalness: value from 0 - 1;` | How metallic the material is from 0 to 1.|
| `Roughness` | `roughness: value from 0 - 1;` | A rougher material will reflect light in more directions than a smooth material.|
| `Spherical Environment Map` | `sphericalEnvMap: #chrome;` | Texture used to give text geometry reflective qualities|


### Asset Item
Default code: <a-asset-item></a-asset-item>

| Attribute        | Code           | Notes  |
| ------------- |:-------------:| -----:|
| `Source` | `src="asset-URL"` | URL of font geometry in assets folder. |
| `Id`| `id="fontname"` | Naming entities makes them easier to use. |

#### Sample code

    <a-asset-item id="robotoregular" 
    src="https://cdn.glitch.com/b383b190-7104-41b5-befc-
    5e511ba62595%2FRobotoRegular.json?1540931389161"></a-asset-item>
      
  
  
### 3D Geometry

| Attribute        | Code           | Notes  |
| ------------- |:-------------:| -----:|
| `Rotation` | `rotation="x y z"` |  X Y Z are coordinates. For more on this go [here](https://aframe.io/docs/0.8.0/guides/building-a-basic-scene.html#transforming-an-entity-in-3d).|
| `Position` | `position="x y z"` | Move your object around your scene.|
| `Scale` | `scale="x y z"` | Scale object by axes x, y & z. |
| `Id`| `id="name"` | Naming objects makes them easier to use. |


### Lights
Default code: `light="`
| Attribute        | Code           | Notes  |
| ------------- |:-------------:| -----:|
| `Type` | `type="ambient"` | One of ambient, directional, hemisphere, point, spot. |
| `Color` | `color="colorname"` | Use names of colors or [hex codes](https://htmlcolorcodes.com/). |
| `Intensity` | `intensity=".5"` | Changes brightness of light.|

#### Sample codes
    <a-entity light="type: ambient; color: white; intensity= .4;">
    </a-entity>
    
   
    <a-entity light="color: red; intensity: .6; type: point;" 
    position="0 0 -10"></a-entity>        



### Entity
Default code: `<a-entity></a-entity>`

| Attribute        | Code           | Notes  |
| ------------- |:-------------:| -----:|
| `Visible` | `visible="true" or "false"` | Hides or shows entity |
| `Id`| `id="entityname"` | Naming entities makes them easier to use. |

#### Sample code
    <a-entity id="freed" visible="true">
        <a-sky src="Free-sky-URL"></a-sky>
        <a-text value="You are free!" position="0 1 -1"></a-text>
      </a-entity>


# Level 3
## Terms and Code

### Plane

Default code: `<a-plane></a-plane>`

| Attribute        | Code           | Notes  |
| ------------- |:-------------:| -----:|
| `Opacity` | `opacity="number on a scale from 0 to 1"` | 0 means fully transparent and 1 means fully opaque. |
| `Rotation` | `rotation="x y z"` |  X Y Z are axis rotation coordinates. For more on this go [here](https://aframe.io/docs/0.8.0/guides/building-a-basic-scene.html#transforming-an-entity-in-3d).|
| `Position` | `position="x y z"` | X Y Z are axis position coordinates|
| `Scale` | `scale="x y z"` | X Y Z scales shape on each axis. |
| `Color` | `color="colorname"` |   Use names of colors or [hex codes](https://htmlcolorcodes.com/).|

#### Sample code
    
    <a-plane position="0 1 -1" color="white" opacity="1"></a-plane>

### Camera
Default code: `<a-camera></a-camera>`

### Cursor
Default code: `<a-cursor fuse="false"></a-cursor>`

### Cursor Events
#### Event-Set
Listens for a set event to occur & executes an assigned action. 

Default code: `event-set__1="`

| Attribute        | Code           | Notes  |
| ------------- |:-------------:| -----:|
| `Click` | `_event: click` | Initiates a targeted action. For more on this go [here](https://aframe.io/docs/0.8.0/components/cursor.html#events). |
| `Target` | `_target: #entity-id` | Defines what the action affects. |
| `Mouse enter` | `_event: mouseenter` | Initiates a targeted action when cursor is over object. |
| `Mouse leave` | `_event: mouseleave` | Initiates a targeted action when cursor is moved off object. |

#### Sample code

  `event-set__1="_event: click; _target: #free; visible: true" event-set__2="_event: click; _target: #trapped; visible: false"`

     
# Level 4
## Terms and Code

### Image
Default code: `<a-image></a-image>`

| Attribute        | Code           | Notes  |
| ------------- |:-------------:| -----:|
| `Source` | `src="image-URL"` | URL of image file in assets folder. |
| `Id`| `id="image-name"` | Naming entities makes them easier to use. |

  
     
# Level 5
## Terms and Code

### Animation
Default code: `<a-animation></a-animation>`


| Attribute        | Code           | Notes  |
| ------------- |:-------------:| -----:|
| `Attribute` | `attribute="attribute-to-animate"` | The attribute you want to animate (scale/position/rotation/etc).|
| `Duration` | `dur="duration-of-animation"` | Duration of animation in milliseconds.|
| `Begin` | `begin="event-name"` |  Event that needs to happen to start the animation.|
| `End` | `end="event-name"` | Event that needs to happen to end the animation.|
| `To` | `to="ending-value"` | Ending value of the attribute being animated.|
| `From` | `from="starting-value"` | Starting value of the attibute being animated. |
| `Repeat` | `repeat="repeat-count" or "indefinite"` | How many times the animation will repeat. |

#### Sample code

  `<a-animation attribute="position" begin="click" dur="1500" to="1 0 2">
  </a-animation>`
  

### Basic 3D Shapes (Primitives)


| Shape        | Code           | 
| ------------- |:-------------:|
| `Box` | `<a-box></a-box>` |
| `Cone` | `<a-cone></a-cone>` |
| `Cylinder` | `<a-cylinder></a-cylinder>` |
| `Sphere` | `<a-sphere></a-sphere>` |
| `Torus` | `<a-torus></a-torus>` |
| `Torus knot` | `<a-torus-knot></a-torus-knot>` |