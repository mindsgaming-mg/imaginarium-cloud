/* Base */

console.log("#MindsGaming");

var tools = document.getElementById("MenuTools");

function menu() {
  tools.classList.toggle("MGmenu");
}

var Gupload = document.getElementById("assets");

function assets() {
  Gupload.classList.toggle("WebSplash");
}

var Gtab = document.getElementById("groups");

function groups() {
  Gtab.classList.toggle("Tabmenu");
}

var Ggames = document.getElementById("games");

function games() {
  Ggames.classList.toggle("Tabmenu");
}

var Gtools = document.getElementById("toolTab");

function toolTab() {
  Gtools.classList.toggle("Tabmenu");
}

function Settings() {
  var settings = document.getElementById("ToolBarMenu");
  settings.classList.toggle("ToolBarMenu");
}

var Toggle = document.getElementById("toolbar");

function Toggletoolbar() {
  Toggle.classList.toggle("Toggletoolbar");
}

var gathering = document.getElementById("gathering");
var pop = document.getElementById("pop");

function POP() {
  gathering.classList("POP");
  pop.classList("unhide");
}

var MGdark = document.getElementById("MGdark");

function DarkMode() {
  MGdark.classList.toggle("MGdark");
}

/* Functions */

var MGmeberships = document.getElementById("memberships");

function memberships() {
  MGmeberships.classList.toggle("WebSplash");
}
