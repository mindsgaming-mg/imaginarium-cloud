# Project Notes / ChangeLog

---

## About The Project

To create a virtual environment together with shared assests in the cloud.

## Issues I've Ran Into

- Canvas covering actions
- Lightweight escape game didn't have full backend
- Click to open page with a virtual UI
- Mobile compatibly
- Running without node

## What I've Learned

- Basic Aframe codes
- How to attach backends
- How to merge backends

## Issues I'm Still Working On

- UI
- Link to page With VR
- Mobile Compatibility

## Accomplishments

- It interacts with actions now
- We made video rooms with 360
- We made a wrap that works with the VR (so far)

## Plans

- Work on UI for the VR
- Build out assets
- Design game layout
- Implement

---

## UI Work-Out In progress

### Home Page Links

[X] VaporRoom

[X] MG

[X] Videos

[X] Code

[X] Gathering

[X] Curry Hobo

### VaporRoom Links

[x] Videos

[x] Gathering

# MG Links

[x] Videos

[x] Code

## Re Orginized

Change folder layout

- [x]Home/Room

- [x]Room/videos

- [x]Room/videos/video.html

- [x]Imaginarium/notes [backup folders]

## CheckList

- [x]Create layout
- [X]Fix Folder Layout
- [X]Fix Page Links
- [x] Create Changelog
- [X ]Push Update - "Clear Cache"

## ChangeLog

- UI
  Created layout for new rooms and content that goes with that room.

- Created catch pages

Kept back up folders for cache issues ; will release after a few days...

---

## New Plans

- Create video page with "Play Next" button or links to room video areas..

- Clean up old backup files / main read.me

- Remove old folders in [Imaginarium]

## Unfinished Plans

- Game Layout

# Current Issues

- Mobile VR

---

## Game Lay-Out Build

### Intro

Alien feeds mushrooms to munkey turning them into "human"

### Growth

The "human" must eat more mushrooms to grow his Mind

### Evolution

Once the human has eaten enough mushrooms it turns into a shawman

### Weapons

Shawman turn mushrooms into "lightbulbs" they can use as weapons

### Building

Shawman can build shelter and other things?

### Enemys

Shawman fight enemies know as "trolls" ,
They defeat the trolls with lightbulbs:

Blue Lightbulb kills Red Trolls

Yellow LightBulb Kills Blue Trolls

## Friendly

Yellow "humans" or shawman

### Levels

... build layout first ...

---

## Game Progress

### Issues

We need to find a good example of an "action" script so assests can be changed/interacte with eachother

### Layout

We can still build the layout and rooms with out this using "hidden links"

### Hidden Links

Hidden links should be placed where the interaction will later accour, actions will relocate to pages for now.

### Needed Assets

Images of backgrounds, enemys, freinds and more!

### Main Building Area

[/game](https://imaginarium-cloud.glitch.me/game/)
