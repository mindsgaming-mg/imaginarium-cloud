# How To Create A Room

These notes go over how to create a room after you have joined or remixed this project.


## Steps

### 1 
Open the 'Imaginarium' folder

### 2
Hover over and click the three dots next to the 'index.html'

## 3
Select 'Duplicate'

### 4
Create or type the folder location and page name 
<code>
FolderName/PageName.html
</code>

## 5
Upload and replace assets:

The index should be well documented so you can upload assets and copy and paste the link into the layout.


## Read Changelog for project plans and changes...